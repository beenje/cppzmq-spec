Summary:     Library for zmq on cpp
Name:        cppzmq
Version:     4.3.1
Release:     3%{?dist}.maxlab
License:     GPL
URL:         http://www.maxlab.lu.se
Group:       System Environment/Libraries
#Source:      %{name}-%{version}.tar.gz
#Source:      https://github.com/zeromq/%{name}/archive/%{version}.tar.gz
Source:      https://github.com/zeromq/%{name}/archive/c6a3529cd167b01a0839de608e19ceba09875317.tar.gz
# for pogo Makefile templates:
BuildRequires:     cmake3
BuildRequires:     zeromq
BuildRequires:     zeromq-devel
BuildRequires:     pkgconfig
Requires(post):    /sbin/ldconfig

%description
cppzmq is a C++ binding for libzmq

%prep
%define zmq_version c6a3529cd167b01a0839de608e19ceba09875317
%setup -q -n %{name}-%{zmq_version}

%build
mkdir build
cd build
cmake3 -DENABLE_DRAFTS=OFF -DCPPZMQ_BUILD_TESTS=OFF ..

%install
[ -z %{buildroot} ] || rm -rf %{buildroot}
cd build
make DESTDIR=%{buildroot} install

%clean
[ -z %{buildroot} ] || rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

# main package includes libraries and copyright info
%files
%defattr (-,root,root,755)
%{_prefix}/local/include/*.hpp
%{_prefix}/local/share/cmake/cppzmq/*

%changelog
* Wed May 15 2019 Aureo Freitas <aureo.freitas@maxiv.lu.se> 4.3.1
- bump version 4.3.1

